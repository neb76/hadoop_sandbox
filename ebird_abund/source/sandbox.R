generate_synth_data <- function(side_length,pp_stixel=50,SRD=F){
	stixel_side <- .5

	stixel_n <- (side_length/stixel_side) ^ 2	
	nnn <- stixel_n * pp_stixel

	if(SRD){
		nnn <- round(sqrt(nnn))
		x <- seq(0,side_length,length.out=nnn)
		y <- seq(0,side_length,length.out=nnn)
		srd.locs <- expand.grid(x,y)
		cur.data <- data.frame(x=srd.locs[,1],
			y=srd.locs[,2])
	}else{
		x <- runif(nnn, min=0, max=side_length)
		y <- runif(nnn, min=0, max=side_length)	
		cur.data <- data.frame(x=x,y=y)

	}
	# --------------
	fixed.time <- 0.375  
	cur.data$t <- runif(nnn, min=fixed.time, max=fixed.time)
	# --------------
	cur.data$r <- sqrt(cur.data$x^2 + cur.data$y^2) + 1e-8
	cur.data$z <- sin(5*pi*cur.data$r*cur.data$t)/cos(cur.data$r * cur.data$t)
	# --------------	
	cur.data$e <- rnorm(length(cur.data$z), mean=0, sd=.5)
	cur.data$obs <- cur.data$z + cur.data$e	
	cur.data <- cbind(1:nrow(cur.data),cur.data)
	return(cur.data)
}

data_2 <- generate_synth_data(2)
str(data_2)
data_4 <- generate_synth_data(4)
str(data_4)
data_6 <- generate_synth_data(6)
str(data_6)
data_8 <- generate_synth_data(8)
str(data_8)
data_10 <- generate_synth_data(10)
str(data_10)

srd_data_2 <- generate_synth_data(SRD=T,side_length=2)
str(srd_data_2)
srd_data_4 <- generate_synth_data(SRD=T,side_length=4)
str(srd_data_4)
srd_data_6 <- generate_synth_data(SRD=T,side_length=6)
str(srd_data_6)
srd_data_8 <- generate_synth_data(SRD=T,side_length=8)
str(srd_data_8)
srd_data_10 <- generate_synth_data(SRD=T,side_length=10)
str(srd_data_10)

# data_2 <- generate_synth_data(2)
# str(data_2)
# data_4 <- generate_synth_data(4)
# str(data_4)
# data_8 <- generate_synth_data(8)
# str(data_8)
# data_16 <- generate_synth_data(16)
# str(data_16)
# data_32 <- generate_synth_data(32)
# str(data_32)
