##4.27
## taking ensemble level results, prepping 

#-------------------------------------------------------------------------------
# for absolute paths on different matchines
#-------------------------------------------------------------------------------
work.station_clue <- Sys.getenv("HOME")
if(work.station_clue== "/Users/nicholasbruns"){
	work.station_root <- "/Users/nicholasbruns/repos/work_repos/"
}else{
	work.station_root <- "/home/fs01/neb76/"
	}

#-------------------------------------------------------------------------------
# set up directory structures, 
#  MSR-- get these paths right...
#-------------------------------------------------------------------------------
library(fields)
library(maps)
run.name <- "TRES_very.small.cali"
ebird_abund_dir <- paste(work.station_root,"hadoop_sandbox/ebird_abund/",sep="")
source(paste(ebird_abund_dir,"source/mapper.3.threshold.testing.library.R",sep=""))
result.dir <- paste(ebird_abund_dir,"runs/",run.name,"/results/hadoop_results/",sep="")
data.dir <- paste(ebird_abund_dir,"runs/",run.name,"/data/",sep="")
#-------------------------------------------------------------------------------
# load and parse results
#-------------------------------------------------------------------------------
# ensemble.level_predictions.2.folds 
fold.n <- 100
results <- read.csv(paste(result.dir,"ensemble.level_predictions.",fold.n,".folds.txt",sep=""))
# names(results) <- c("data.type","orig.row.id","lon","lat","date","obs","mean","median","q.10","q.90")
names(results) <- c("data.type","orig.row.id","lon","lat","date","obs",
	"pi.mean","pi.median","pi.q.10","pi.q.90",
	"tau.mean","tau.median","tau.q.10","tau.q.90",
	"mu.mean","mu.median","mu.q.10","mu.q.90",
	"truncated.mean","truncated.median","truncated.q.10","truncated.q.90",
	"pi.mu.mean","pi.mu.median","pi.mu.q.10","pi.mu.q.90")
results$pi.mu.q.90 <- as.numeric(as.character(results$pi.mu.q.90)) # last column always seems to get casted to factor
results_by.type <- split(results,results$data.type)
#-------------------------------------------------------------------------------
# make data set frame
# locs, observation, prediction
## use the out of bag mean!
#-------------------------------------------------------------------------------
thresh_column_vec <- c("lon","lat","date","obs","pi.mean")
thresh_test_frame <- results_by.type$train.out.bag[,thresh_column_vec]	
names(thresh_test_frame)[5] <- "pred"
paste(data.dir,"ebird.abund_",run.name,"__data.frame.csv",sep="")
write.csv(thresh_test_frame,
	paste(data.dir,"ebird.abund_",run.name,"_occurence.results_data.frame.csv",sep="")	
	)  ##here, put the extacted part somewhere all nodes can see (this reduced set, just a convenience, to avoid loading larger file in)
#-------------------------------------------------------------------------------
# prep source data for the threshold testing
# place on hdfs
#-------------------------------------------------------------------------------
MC.DRAWS.N <- 25
THRESH.CANDIDATE.N <- 100



#-------------------------------------------------------------------------------
# make the iterations frame
# <week.id> <thresh.cand> <mc.iteration>
## done second, because needs knowledge of the test data to propose threshold?
#-------------------------------------------------------------------------------
all_mc_frame <- data.frame()
for(week.id in 1:52){
# for(week.id in 7:9){
	month_block <- extract_month_block(week.id,thresh_test_frame)
	if(nrow(month_block)== 0 ) next
	candidate_thresholds <- propose_thresholds(month_block$pred,month_block$obs)	
	week_frame <- expand.grid(mc.draw=1:MC.DRAWS.N,thresh.cand=candidate_thresholds)
	week_frame$week.id <- week.id	
	all_mc_frame <- rbind(all_mc_frame,week_frame)
}

write.table(all_mc_frame,
	file= paste(data.dir,"ebird.abund_",run.name,"_threshold.test.instance_data.frame.csv",sep=""),
	row.name=F,
	col.names=F,
	sep=",")
#-------------------------------------------------------------------------------
# funcitons to be moved
#-------------------------------------------------------------------------------
