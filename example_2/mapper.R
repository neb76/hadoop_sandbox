#! /usr/bin/env Rscript 
##/usr/lib64/R/bin/Rscript
# example2 mapper
#sort by parition!
#recall, data is 6 colum:
	# x 	y	t=3.75	radius	z=(obs + e)  obs
source('/home/fs01/neb76/hadoop_sandbox/example_2/vMR.library.R')
load('/home/fs01/neb76/hadoop_sandbox/example_2/partition.1.RData')
# source('vMR.library.R')
# load('partition.1.RData')
input <- file( "stdin" , "r" )
#input <- file("boston_2.csv","r")

while( TRUE ){
	 currentLine <- readLines( input , n=1 ) 
	 if( 0 == length( currentLine ) ){
	 	break
	 }

	 currentFields <- unlist( strsplit( currentLine , "," ) ) 
	 #remember, these come through as characters, so caste into numeric!
	 cur.loc <- data.frame(x=as.numeric(currentFields[1]),
	 	y=as.numeric(currentFields[2]))
	 stixel_id <- RectangularPartitioning(cur.loc,partition) 
	 if(is.na(stixel_id)) stixel_id <- 0
	 result <- paste(stixel_id,currentLine,sep="\t")  #i think need an extra, because quotes moved in...

	 cat( result , "\n" , sep="" ) 
}
close( input )
