work.station_clue <- Sys.getenv("HOME")
if(work.station_clue== "/Users/nicholasbruns"){
	work.station_root <- "/Users/nicholasbruns/repos/work_repos/"
}else{
	work.station_root <- "/home/fs01/neb76/"
	}

profile_1_dir <- paste(work.station_root,"hadoop_sandbox/profile_1/",sep="")

all_args<-commandArgs(trailingOnly=T)		
trim<-function(p_string){
	return(gsub("(^ +)|( +$)", "", p_string))
}

# side_len <- 1.5
# sample_n <- 4
# rep_n <- 2
# fold_n <- sample_n * rep_n

side_len <- as.numeric(trim(all_args[1]))
sample_n <- as.numeric(trim(all_args[2]))
rep_n <- as.numeric(trim(all_args[3]))
fold_n <- sample_n * rep_n

run_dir <- paste(profile_1_dir,"runs/side.length.",side_len,"/",sep="")
baseline_dir <- paste(run_dir,"baseline.results/",sep="")


train.data <- read.csv(paste(run_dir,"data/stem_train.data_side.len.",side_len,".csv",sep=""),header=F)
names(train.data) <- c("row_id","x","y","t","r","z","e","obs","type")
test.data <- read.csv(paste(run_dir,"data/stem_test.data_side.len.",side_len,".csv",sep=""),header=F)
names(test.data) <- c("row_id","x","y","t","r","z","e","obs","type","bs.sample")
srd.data <- read.csv(paste(run_dir,"data/stem_srd.data_side.len.",side_len,".csv",sep=""),header=F)
names(srd.data) <- c("row_id","x","y","t","r","z","e","obs","type","bs.sample")


source(paste(profile_1_dir,"source/vMR.library.R",sep=""))
source(paste(profile_1_dir,"source/vMR_baseModels.12.05.R",sep=""))

###do the sampling!
#-------------------------------------------------------------------------------
# model fitting and prediction!
#-------------------------------------------------------------------------------
train.locations <- train.data[,2:3]
test.locations <- test.data[,2:3]
srd.locations <- srd.data[,2:3]

dir.create(baseline_dir)
cur.fold.id <- 1
for(sample.iii in 1:sample_n){
	cur_bs.sample <- read.csv(paste(run_dir,"data/bs.sample.",sample.iii,"_side.len.",side_len,".csv",sep=""),header=F)
	cur_bs.sample <- cur_bs.sample[,1]
	for(rep.ii in 1:rep_n){
		cat("currently fitting fold: ", cur.fold.id,"\n")
		cur_train.data <- train.data[cur_bs.sample,2:8]
		cur_train.locations <- train.data[cur_bs.sample,2:3]
		d.stem <- stem(
			data=cur_train.data,
			locations=cur_train.locations,
			width = c(.5,.5),
			n=1,
			baseModel=GAM_train,
			bm.formula="obs ~ x +y",
			min.data.size=10
			)

		train.preds <- predict.stem(
			stem.obj=d.stem,
			new.data=train.data,
			new.locations=train.locations,
			baseModel=GAM_predict
			)

		save(train.preds, file=
			paste(baseline_dir,"train.preds_fold.",cur.fold.id,".RData",sep="")
			)
		
		test.preds <- predict.stem(
				stem.obj=d.stem,
				new.data=test.data,
				new.locations=test.locations,
				baseModel=GAM_predict
				)

		save(test.preds, file=
			paste(baseline_dir,"test.preds_fold.",cur.fold.id,".RData",sep="")
			)

		srd.preds <- predict.stem(
				stem.obj=d.stem,
				new.data=srd.data,
				new.locations=srd.locations,
				baseModel=GAM_predict
				)
		save(srd.preds, file=
			paste(baseline_dir,"srd.preds_fold.",cur.fold.id,".RData",sep="")
			)

		cur.fold.id <- cur.fold.id + 1
	}

}



#-------------------------------------------------------------------------------
# now do the reassembly
#-------------------------------------------------------------------------------
##for now, just do it flat.
n.train <- length(readLines(paste(run_dir,"data/stem_train.data_side.len.",side_len,".csv",sep=""))) 
n.test <- length(readLines(paste(run_dir,"data/stem_test.data_side.len.",side_len,".csv",sep=""))) 
n.srd <- length(readLines(paste(run_dir,"data/stem_srd.data_side.len.",side_len,".csv",sep=""))) 

cum_train <- rep(0,n.train)
cum_test <- rep(0,n.test)
cum_srd <- rep(0,n.srd)
count_train <- rep(0,n.train)
count_test <- rep(0,n.test)
count_srd <- rep(0,n.srd)

for(cur.fold.id in 1:fold_n){
	cat('on fold:',cur.fold.id,"\n")
	load(paste(baseline_dir,"train.preds_fold.",cur.fold.id,".RData",sep=""))	#train.preds
	train_not.na <- !is.na(train.preds$mean)	
	count_train <- count_train + as.numeric(train_not.na)
	cum_train[train_not.na] <- cum_train[train_not.na] + train.preds$mean[train_not.na]

	test_not.na <- !is.na(test.preds$mean)	
	count_test <- count_test + as.numeric(test_not.na)
	cum_test[test_not.na] <- cum_test[test_not.na] + test.preds$mean[test_not.na]

	srd_not.na <- !is.na(srd.preds$mean)	
	count_srd <- count_srd + as.numeric(srd_not.na)
	cum_srd[srd_not.na] <- cum_srd[srd_not.na] + srd.preds$mean[srd_not.na]
}

train_zero_count <- count_train < 1
train.preds <- rep(NA,n.train)
train.preds[!train_zero_count] <- cum_train[!train_zero_count]/count_train[!train_zero_count]

test_zero_count <- count_test < 1
test.preds <- rep(NA,n.test)
test.preds[!test_zero_count] <- cum_test[!test_zero_count]/count_test[!test_zero_count]

srd_zero_count <- count_srd < 1
srd.preds <- rep(NA,n.srd)
srd.preds[!srd_zero_count] <- cum_srd[!srd_zero_count]/count_srd[!srd_zero_count]


train.ave <- data.frame(
	x=train.data$x,
	y=train.data$y,
	count=count_train,
	mean=train.preds,
	obs=train.data$obs
	)

test.ave <- data.frame(
	x=test.data$x,
	y=test.data$y,
	count=count_test,
	mean=test.preds,
	obs=test.data$obs
	)

srd.ave <- data.frame(
	x=srd.data$x,
	y=srd.data$y,
	count=count_srd,
	mean=srd.preds,
	obs=srd.data$obs
	)



write.csv(train.ave,file=
	paste(baseline_dir,"train.pred.ave.csv",sep=""))

write.csv(test.ave,file=
	paste(baseline_dir,"test.pred.ave.csv",sep=""))

write.csv(srd.ave,file=
	paste(baseline_dir,"srd.pred.ave.csv",sep=""))


train.rmse <- sqrt(mean((train.data$obs - train.ave$mean)^2,na.rm=T))
cat('rmse on training data: ', train.rmse, "\n")




