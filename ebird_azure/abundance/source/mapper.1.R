#! /usr/bin/Rscript
##! /usr/lib64/R/bin/Rscript
##! /usr/bin/env Rscript 


#########    /usr/lib64/R/bin/Rscript #ATLAS
#########    /usr/bin/env Rscript  #local
options(warn=-1)
source("mapper.1.partitioning.library.R")

# run.name <- Sys.getenv("RUN_NAME")
# load(paste(ebird_abund_dir,'runs/',run.name,'/data/partition.objects_list.RData',sep=""))
load('partition.objects_list.RData')

# source('vMR.library.R')
# load('partition.1.RData')
input <- file( "stdin" , "r" )
#input <- file("boston_2.csv","r")

#-------------------------------------------------------------------------------
# mapping params! (experimental design)
#-------------------------------------------------------------------------------
REP_N <- as.numeric(Sys.getenv("REP_N"))
SAMPLE_N <- as.numeric(Sys.getenv("SAMPLE_N"))
# REP_N <- 2
# SAMPLE_N <- 2
SUB_SAMPLE_P <- .7
data.type_position <- 29
data.date_position <- 4


DEBUG <- T
if(DEBUG){
	SRD_DATE_VEC <- c(.24,.26)  ##2 weeks for the debug case! exactly a week apart!	
}else{
	SRD_DATE_VEC <- seq(from = 0, to= 1, length= 52 +1)
	SRD_DATE_VEC <- (SRD_DATE_VEC[1:SRD_LENGTH] + SRD_DATE_VEC[2:(SRD_LENGTH+1)])/2	
}

#-------------------------------------------------------------------------------
# mapping outine!
#-------------------------------------------------------------------------------
while( TRUE ){
	 currentLine <- readLines( input , n=1 ) 
	 if( 0 == length( currentLine ) ){
	 	break
	 }

	 currentFields <- unlist( strsplit( currentLine , "," ) ) 
	 #remember, these come through as characters, so caste into numeric!
	 ##currentFields
	 	# "row_id","x","y","t","r","z","e","obs","type"
	 # "1 row_id" , "2 x" , "3 y" , "4 t" , "5 r" , "6 z" , "7 e" , "8 obs" , "9 type"
	 circle_date <- as.numeric(currentFields[4])
	 circle_date <- circle_date - floor(circle_date) #here, enforcing year agreggation
	 												# pull this out if you're trying to do yearly's
	 cur.loc <- data.frame(lon=as.numeric(currentFields[2]),
	 	lat=as.numeric(currentFields[3]),
	 	date=circle_date) 

	 fold.iii <- 0
	 for(sample.iii in 1:SAMPLE_N){	
	 	cur_sample_indicator <- as.logical(rbinom(1,1,SUB_SAMPLE_P))
		 for(rep.iii in 1:REP_N){
			 fold.iii <- fold.iii + 1
			 #-------------------------------------------------------------------------------
			 #  now decipher membership and write out the result
			 #  if you're an srd oint, repeat for all prediction time points! probably 52 times
			 #-------------------------------------------------------------------------------
			 ##if SRD point, iterate through the weeks, ie 52 weeks
			 if(currentFields[data.type_position]=="\"srd\""){
			 	for(cur_srd_date in SRD_DATE_VEC){
			 		##replace the NA with a specific date!
			 		cur.loc$date <- cur_srd_date
			 		stixel_id <- RectangularPartitioning(cur.loc,partition.objs_list[[fold.iii]]) ## possible optimization: use lapply and operate on the list instead of looping through..
			 		if(is.na(stixel_id)) stixel_id <- 0
			 		stixel_id <- paste(fold.iii,stixel_id,sep="-") ## add fold identifying

			 		##not so easy! must reparse the date string!
			 		currentFields[4] <- cur_srd_date
			 		reparsed.line_with.date <- paste(currentFields,collapse=",")
			 		reparsed.line_with.date <- paste(reparsed.line_with.date,"TRUE",sep=",")

			 		result <- paste(stixel_id,reparsed.line_with.date,sep="\t")  #i think need an extra, because quotes moved in..
				 	cat( result , "\n" , sep="" ) 
				 }
			 }else{
			 	#-------------------------------------------------------------------------------
			 	# perform subsampling (only relevent for training points)
			 	#-------------------------------------------------------------------------------
				 with_sample_indicator <- currentLine
				 if(currentFields[data.type_position]=="\"train\""){ 
					 with_sample_indicator <- paste(with_sample_indicator,cur_sample_indicator,sep=",")	 ##add subsample id!
				 } else{
				 	##put a dummy value, T, on test and srd data!
					 with_sample_indicator <- paste(with_sample_indicator,"TRUE",sep=",")	 ##add subsample id!
				 }

				 stixel_id <- RectangularPartitioning(cur.loc,partition.objs_list[[fold.iii]]) ## possible optimization: use lapply and operate on the list instead of looping through..
				 if(is.na(stixel_id)) stixel_id <- 0
				 stixel_id <- paste(fold.iii,stixel_id,sep="-") ## add fold identifying

				 result <- paste(stixel_id,with_sample_indicator,sep="\t")  #i think need an extra, because quotes moved in..
				 cat( result , "\n" , sep="" ) 
			 }

			 ##now do the next fold!  remember, thats just a differnt random paritioning with which to sort this point
		 } ##close replicate loop
	 }## close sample loop
} ## done processing all lines!
close( input )
