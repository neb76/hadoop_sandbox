#! /usr/lib64/R/bin/Rscript
### /home/fs01/neb76/old.R/R-2.15.3/bin/Rscript
#sort by tax group!
input <- file( "stdin" , "r" )
#input <- file("boston_2.csv","r")

while( TRUE ){
	 currentLine <- readLines( input , n=1 ) 
	 if( 0 == length( currentLine ) ){
	 	break
	 }

	 currentFields <- unlist( strsplit( currentLine , "," ) ) 
	 result <- paste(currentFields[10],currentLine,sep="\t")  #i think need an extra, because quotes moved in...

	 cat( result , "\n" , sep="" ) 
}
close( input )
